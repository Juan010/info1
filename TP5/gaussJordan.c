//TP5: Gauss Jordan
// Ax + By + Cz = D
// Ex + Fy + Gz = H
// Ix + Jy + Kz = L
#include <stdio.h>
void printMatrix(float mat[][80], int x, int y){

	for(int i = 0; i < y; i++){
		for(int j = 0; j < x; j++)
			printf("%.10f ", mat[i][j]);
		printf("\n");
	}
	printf("\n");
}
void gaussJordan(float mat[][80], int nVar, int nEq){

	for(int i = 0; i < nEq; i++)
		for(int j = 0; j < nVar; j++){
			if (i == j){
				
				float saveVar = mat[i][j];
				for(int k = 0; k <= nVar; k++)
					mat[i][k] /= saveVar;
				printMatrix(mat, nVar+1, nEq); 	
				for(int k = 0; k < nEq; k++){
					if(k == i)
						continue;
					saveVar = -mat[k][j];
					for(int l = 0; l <= nVar; l++)
						mat[k][l] += mat[i][l] * saveVar;
				 }
				printMatrix(mat, nVar+1, nEq); 	
			}
		}
}


int main(void){
	float matrizEntrada[80][80] = {{0}};
	int nEcuaciones, nVariables;
	printf("Ingrese la cantidad de variables de su matriz: ");
	scanf("%d", &nVariables);
	printf("Ingrese la cantidad de ecuaciones de su matriz: ");
	scanf("%d", &nEcuaciones);
	for(int i = 0; i < nEcuaciones; i++)
		for(int j = 0; j <= nVariables; j++){
			if (j != nVariables)
				printf("Ingrese el coeficiente de la Variable %d de la Ecuacion %d: ", j+1, i+1);
			else
				printf("Ingrese el valor del termino independiente de la Ecuacion %d: ", i+1);
			scanf("%f", &matrizEntrada[i][j]);
		}
		
	printMatrix(matrizEntrada, nVariables + 1, nEcuaciones);
	gaussJordan(matrizEntrada, nVariables, nEcuaciones);
	printMatrix(matrizEntrada, nVariables + 1, nEcuaciones);

	return 0;
}
