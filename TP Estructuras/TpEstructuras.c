#include <stdio.h>
#define N 100
struct estudiante{
	unsigned int legajo;
	char nombre[80];
};

void cargar_estudiantes(struct estudiante v[], int n){
    for(int i = 0; i < n; i++){
        printf("\nEstudiante Nº %d\n\n", i + 1);
        printf("Ingrese el legajo del estudiante: ");
        scanf("%u", &v[i].legajo);
        printf("Ingrese el nombre del estudiante: ");
        scanf(" %s", v[i].nombre);
    }
}
        
void ordenar(struct estudiante v[], int n){
    for(int j = 0; j < n - 1; j++)
        for(int i = 0; i < n - 1; i++)
            if(v[i].legajo > v[i+1].legajo){
                struct estudiante tmp = v[i];
                v[i] = v[i+1];
                v[i+1] = tmp;
            }   
}
void imprimir(struct estudiante v[], int n){
    for (int i = 0; i < n; i++){
        printf("Estudiante: %d\n\n", i+1);
        printf("Legajo: %u\n", v[i].legajo);
        printf("Nombre: %s\n\n", v[i].nombre);
    }
}


int main(void){
    struct estudiante alumnos[N];
    
    int n;
    printf("cuantos? ");
    scanf("%u", &n);
    cargar_estudiantes(alumnos, n);
    ordenar(alumnos, n);
    imprimir(alumnos, n);
    return 0;
}

